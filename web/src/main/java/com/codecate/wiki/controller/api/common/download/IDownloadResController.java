package com.codecate.wiki.controller.api.common.download;

import java.io.File;
import java.net.URLDecoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.codecate.wiki.common.utils.IHttpFileUtils;
import com.codecate.wiki.controller.api.common.IBaseController;
import com.codecate.wiki.service.common.download.IDownloadServiceImpl;

/**
 * 资源下载接口
 */
@Controller
@RequestMapping("/blog/download")
public class IDownloadResController extends IBaseController {

    @Autowired
    IDownloadServiceImpl service;

    @Value("${cus.res.file.basepath}")
    private String cusResFileBasePath;

    @SuppressWarnings("deprecation")
    @RequestMapping("/res/**")
    public void file(HttpServletRequest req, HttpServletResponse resp) {
        try {
            String uri = req.getRequestURI();
            String contenxtPath = req.getContextPath();
            String uriPre = contenxtPath + "/blog/download/res/";
            String result = uri.replaceAll(uriPre, "");
            String path = this.cusResFileBasePath + result;
            path = URLDecoder.decode(path);
            File file = new File(path);
            if (file.exists()) {
                IHttpFileUtils.downloadFileWithFileName(file, file.getName(), req, resp);
            } else {
                resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
//				return null;
            }
        } catch (Exception e) {
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            e.printStackTrace();
//			return null;
        }
    }

    @RequestMapping("/img/resize/**")
    public void imgResize(HttpServletRequest req, HttpServletResponse resp) {
        try {
            String uri = req.getRequestURI();
            String contenxtPath = req.getContextPath();
            String uriPre = contenxtPath + "/blog/download/img/resize/";
            String result = uri.replaceAll(uriPre, "");
            String size = req.getParameter("size");
            String mode = req.getParameter("mode");
            String format = req.getParameter("format");
            Long width = 0L;
            Long height = 0L;
            if (null != size) {
                String[] sizeArr = size.split("_");
                width = Long.parseLong(sizeArr[0]);
                height = Long.parseLong(sizeArr[1]);
            }
            String filePath = service.changeImgSize(result, width, height, mode, format);
            File file = new File(filePath);
            if (file.exists()) {
                IHttpFileUtils.download(file, req, resp);
            } else {
                resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }
        } catch (Throwable e) {
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            e.printStackTrace();
        }
    }

}