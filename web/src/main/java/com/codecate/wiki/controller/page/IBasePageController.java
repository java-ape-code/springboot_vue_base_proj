package com.codecate.wiki.controller.page;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.thymeleaf.context.WebContext;

import lombok.Data;

@Data
public class IBasePageController {

	@Value("${cus.common.host}")
	String urlHost;
	
	protected void addPageValue(HttpServletRequest req, HttpServletResponse resp, Map<String, Object> model) {
		
		WebContext ctx = new WebContext(req, resp, req.getServletContext(), req.getLocale(), model);
		ctx.setVariable("baseUrl", urlHost);
		model.put("baseUrl", urlHost);
		
	}

}
