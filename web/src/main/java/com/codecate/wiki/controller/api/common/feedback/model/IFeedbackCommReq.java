package com.codecate.wiki.controller.api.common.feedback.model;

import lombok.Data;

/**
 * IFeedbackCommReq
 */
@Data
public class IFeedbackCommReq {

    private String appKey;
    private String content;

    
}