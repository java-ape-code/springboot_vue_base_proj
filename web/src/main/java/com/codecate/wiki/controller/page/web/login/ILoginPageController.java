package com.codecate.wiki.controller.page.web.login;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.codecate.wiki.controller.page.IBasePageController;

@Controller
public class ILoginPageController extends IBasePageController{

	@RequestMapping("/login")
	public String login(HttpServletRequest req, HttpServletResponse resp, Map<String, Object> model) {
		addPageValue(req, resp, model);
		return "admin/login";
		
	}
	
}
