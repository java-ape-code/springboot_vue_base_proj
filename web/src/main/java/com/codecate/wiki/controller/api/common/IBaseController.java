package com.codecate.wiki.controller.api.common;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import com.codecate.wiki.common.constant.IConstants;
import com.codecate.wiki.common.entity.common.IAppUser;
import com.codecate.wiki.service.common.user.IAppUserServiceImpl;

public class IBaseController {

	@Autowired
	IAppUserServiceImpl iAppUserServiceImpl;

	String  getServerBasePath(HttpServletRequest req){
		String path = req.getContextPath();
		String host = req.getServerName()+":"+req.getServerPort();
		if(req.getServerPort() == 80) {
			host = req.getServerName();
		}
		String basePath = req.getScheme()+"://"+host+path+"";
		return basePath;
	}

	public IAppUser getAppUser(HttpServletRequest req) {
		IAppUser user =  (IAppUser) req.getAttribute(IConstants.AppUser.LOGIN_USER_SESSION_KEY);
		if (null==user) {
			String token = this.getToken(req);
			Long userId = this.getUserId(req);
			user = this.iAppUserServiceImpl.checkUserIdAndToken(userId, token);
		}
		return user;
	}

	public String getToken(HttpServletRequest req) {
		return req.getHeader("token");
	}

	public Long getUserId(HttpServletRequest req) {
		String userIdStr = req.getHeader("id");
		if(null==userIdStr){
			return 0L;
		}
		return Long.parseLong(userIdStr);
	}
	
}
