package com.codecate.wiki.controller.api.common.feedback;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.codecate.wiki.common.model.common.IRespStatus;
import com.codecate.wiki.controller.api.common.IBaseController;
import com.codecate.wiki.controller.api.common.feedback.model.IFeedbackCommReq;
import com.codecate.wiki.service.common.feedback.IFeedbackServiceImpl;

/**
 * 用户反馈
 */


@Controller
@RequestMapping("/api/v1")
public class IFeedbackController extends IBaseController{

    @Autowired
    IFeedbackServiceImpl iFeedbackServiceImpl;

    @RequestMapping("/feedback")
    @ResponseBody
    public Object feedback(@RequestBody IFeedbackCommReq iReq, HttpServletRequest req, HttpServletResponse resp){
        IRespStatus status = new IRespStatus();
        this.iFeedbackServiceImpl.save(iReq.getAppKey(), iReq.getContent());
        return status;
    }


    
}