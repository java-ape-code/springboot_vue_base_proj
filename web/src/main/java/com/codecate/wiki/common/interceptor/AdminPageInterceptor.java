package com.codecate.wiki.common.interceptor;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.codecate.wiki.common.utils.IRedisUtil;

@Component
public class AdminPageInterceptor implements HandlerInterceptor {

    @Value("${cus.common.host}")
    String urlHost;
    @Autowired
    IRedisUtil redisUtil;

    @Override
    public void afterCompletion(HttpServletRequest req, HttpServletResponse resp, Object arg2, Exception arg3)
            throws Exception {
    }

    @Override
    public void postHandle(HttpServletRequest req, HttpServletResponse resp, Object arg2, ModelAndView arg3)
            throws Exception {
    }

    @Override
    public boolean preHandle(HttpServletRequest req, HttpServletResponse resp, Object arg2) throws Exception {
        String token = this.getTokenFromReq(req);
        if (StringUtils.isBlank(token)) {
            resp.setStatus(401);
            return false;
        }
        String userJson = this.redisUtil.get(token);
        if (StringUtils.isBlank(userJson)) {
            resp.setStatus(403);
            return false;
        }
        return true;
    }

    public String getTokenFromReq(HttpServletRequest req) {
        String result = req.getHeader("X-Token");
        if (StringUtils.isNotBlank(result)) {
            return result;
        }
        result = valueFromCookie("X-Token",req.getCookies());
        if (StringUtils.isNotBlank(result)) {
            return result;
        }
        result = valueFromCookie("token",req.getCookies());
        if (StringUtils.isNotBlank(result)) {
            return result;
        }
        return "";
    }

    public Long getUserIdFromReq(HttpServletRequest req) {
        Cookie[] cookies = req.getCookies();
        String result = valueFromCookie("userId", cookies);
        if (null != result) {
            return Long.parseLong(result);
        }
        return 0L;
    }

    private String valueFromCookie(String key, Cookie[] cookies) {
        if (null != cookies && cookies.length > 0) {
            for (Cookie cookie : cookies) {
                if (key.equalsIgnoreCase(cookie.getName())) {
                    return cookie.getValue();
                }
            }
        }
        return null;
    }

}
