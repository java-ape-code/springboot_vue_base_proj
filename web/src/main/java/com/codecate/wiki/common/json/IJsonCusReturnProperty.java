package com.codecate.wiki.common.json;

import java.util.List;

public interface IJsonCusReturnProperty {

    List<String> jsonCusNoReturnPropertyNameList();

}
