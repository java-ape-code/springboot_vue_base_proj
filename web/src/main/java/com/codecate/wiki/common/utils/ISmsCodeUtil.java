package com.codecate.wiki.common.utils;

import java.util.Random;

public class ISmsCodeUtil {

	public static String createSmsCode(){
		Random random = new Random();
		
		String result = "";
		int i = 0;
		while (i<4) {
			int tmp = random.nextInt()%10;
			result = result+tmp;
			i++;
		}
		return result;
		
	}
	
	
	
}
