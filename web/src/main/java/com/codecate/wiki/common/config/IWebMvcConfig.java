package com.codecate.wiki.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.codecate.wiki.common.interceptor.AdminPageInterceptor;
import com.codecate.wiki.common.interceptor.IYcysAppNeedLoginInterceptor;
import com.codecate.wiki.common.json.IJsonPropertyPreFilter;

@Configuration
public class IWebMvcConfig implements WebMvcConfigurer{

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        InterceptorRegistration addInterceptor = registry.addInterceptor(getAdminPageInterceptor());
        addInterceptor.addPathPatterns("/admin/**");
        addInterceptor.excludePathPatterns("/admin/login");
        addInterceptor.excludePathPatterns("/admin/api/v1/user/login");
        addInterceptor.excludePathPatterns("/admin/js/**");
        addInterceptor.excludePathPatterns("/admin/css/**");
        addInterceptor.excludePathPatterns("/admin/fonts/**");
        addInterceptor.excludePathPatterns("/admin/images/**");
        addInterceptor.excludePathPatterns("/admin/img/**");
       

        InterceptorRegistration ycysloginCheckInterceptor = registry.addInterceptor(getYcysLoginCheckInterceptor());
        ycysloginCheckInterceptor.addPathPatterns("/ycys/api/v1/user/**");
        ycysloginCheckInterceptor.addPathPatterns("/yssp/api/v1/user/**");
        
        ycysloginCheckInterceptor.excludePathPatterns("/ycys/api/v1/user/sendcode");
        ycysloginCheckInterceptor.excludePathPatterns("/ycys/api/v1/user/login");
        ycysloginCheckInterceptor.excludePathPatterns("/ycys/api/v1/user/token/login");
        ycysloginCheckInterceptor.excludePathPatterns("/ycys/api/v1/user/smslogin");
        ycysloginCheckInterceptor.excludePathPatterns("/ycys/api/v1/user/register");
        ycysloginCheckInterceptor.excludePathPatterns("/ycys/api/v1/user/resetpwd");
        ycysloginCheckInterceptor.excludePathPatterns("/ycys/api/v1/user/threelogin");
        
    }

    @Bean
    public AdminPageInterceptor getAdminPageInterceptor() {
        return new AdminPageInterceptor();
    }

    @Bean
    public IYcysAppNeedLoginInterceptor getYcysLoginCheckInterceptor(){
        return new IYcysAppNeedLoginInterceptor();
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        FastJsonHttpMessageConverter fastConvert = new FastJsonHttpMessageConverter();
        FastJsonConfig fastJsonConfig = new FastJsonConfig();
        fastJsonConfig.setSerializerFeatures(
                SerializerFeature.WriteNullStringAsEmpty
                , SerializerFeature.WriteMapNullValue
                , SerializerFeature.WriteNullListAsEmpty
                , SerializerFeature.WriteNullNumberAsZero
                , SerializerFeature.WriteNullBooleanAsFalse
                , SerializerFeature.DisableCircularReferenceDetect
        );
        List<MediaType> fastMediaTypes = new ArrayList<>();
        fastMediaTypes.add(MediaType.APPLICATION_JSON_UTF8);
        fastConvert.setSupportedMediaTypes(fastMediaTypes);
        fastJsonConfig.setSerializeFilters(new IJsonPropertyPreFilter());
        fastConvert.setFastJsonConfig(fastJsonConfig);
        converters.add(0, fastConvert);
    }
    
}
