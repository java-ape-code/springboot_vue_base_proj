package com.codecate.wiki.common.dao.common;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.codecate.wiki.common.dao.IBaseDao;
import com.codecate.wiki.common.entity.common.IRes;
import com.codecate.wiki.common.mapper.common.IResMapper;
import com.codecate.wiki.common.model.common.IWebPage;
import com.codecate.wiki.common.utils.ITimeUtils;

import org.springframework.stereotype.Component;

/**
 * IResDao
 */
@Component
public class IResDao extends IBaseDao<IResMapper,IRes>{

    public void saveRes(IRes res){
        res.setCreateTime(ITimeUtils.currentTimeSecond());
        this.insert(res);
    }

    public void delResById(Long id){
        IRes res = new IRes();
        res.setId(id);
        res.setFlag(0L);
        this.update(res);
    }

    public void updateRes(IRes res){
        this.update(res);
    }

	public List<IRes> findResByMd5(String md5) {
        IRes entity = new IRes();
        entity.setMd5(md5);
        entity.setFlag(1L);
        return this.selectListByEntity(entity);
	}

	public IWebPage<IRes> findResList(int pageNum, int pageDataNum) {
        QueryWrapper<IRes> qw = new QueryWrapper<IRes>();
        qw.eq("flag", 1L);
        qw.orderByDesc("id");
        return this.selectByPageNumByQW(pageNum, pageDataNum, qw);
    }
    
    public List<IRes> needCover(){
        return this.baseMapper.resListNeedCover();
    }
    
    public List<IRes> resByIds(List<Long> resIds){
    	return this.baseMapper.resByIds(resIds);
    }
    
    public IRes resById(Long id) {
    	return this.selectByPrimaryKey(id);
    }

}