package com.codecate.wiki.common.mapper.common;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.codecate.wiki.common.entity.common.IRes;
import com.codecate.wiki.common.mapper.IBaseMapper;

/**
 * IResMapper
 */
public interface IResMapper extends IBaseMapper<IRes>{


    @Select({"SELECT * FROM t_res where cover_flag=0 order by id desc limit 0,100"})
	public List<IRes> resListNeedCover();
    

    @Select({ "<script>select * from t_res where id in "
                    + "<foreach collection='resIdList' index='index' item='item' open='(' separator=',' close=')'>"
                    + " #{item}" + " </foreach>" + " </script>" })
    public List<IRes> resByIds(@Param("resIdList") List<Long> ids);



    
}