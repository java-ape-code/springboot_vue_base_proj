package com.codecate.wiki.common.mapper.common;

import com.codecate.wiki.common.entity.common.IFeedback;
import com.codecate.wiki.common.mapper.IBaseMapper;

/**
 * IFeedbackMapper
 */
public interface IFeedbackMapper extends IBaseMapper<IFeedback>{

    
}