package com.codecate.wiki.common.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.codecate.wiki.common.mapper.IBaseMapper;
import com.codecate.wiki.common.model.common.IWebPage;

/**
 * 公共dao
 * @author rui
 *
 * 2019年5月20日
 *
 */

public abstract class IBaseDao<M extends IBaseMapper<E>, E>{
	
	@Autowired
    protected M baseMapper;
	
	protected M getDao() {
		return this.baseMapper;
	}
	
	protected void insert(E entity){
		this.getDao().insert(entity);
	}
	
	protected void update(E entity) {
		this.getDao().updateById(entity);
	}
	
	protected void update(E entity, Wrapper<E> updateWrapper){
		this.getDao().update(entity, updateWrapper);
	}
	
	protected E selectOne(Wrapper<E> queryWrapper){		
		return this.getDao().selectOne(queryWrapper);
	}
	
	protected E selectOneByEntity(E entity){
		return this.getDao().selectOne(new QueryWrapper<E>(entity));
	}
	
	protected E selectByPrimaryKey(Long id){
		return this.getDao().selectById(id);
	}
	
	protected List<E> selectList(Wrapper<E> queryWrapper){
		return getDao().selectList(queryWrapper);
	}
	
	protected List<E> selectListByEntity(E entity){
		return this.getDao().selectList(new QueryWrapper<E>(entity));
	}
	
	protected List<E> selectByIds(List<Long> ids){
		QueryWrapper<E> queryWrapper = new QueryWrapper<E>();
		queryWrapper.in("id", ids);
		List<E> result = this.getDao().selectList(queryWrapper);
		return result;
	}
	
	protected List<E> selectAll(){
		return getDao().selectList(null);
	}
	
	protected Integer selectAllCount(){
		return getDao().selectCount(null);
	}
	
	protected Integer selectCount(Wrapper<E> queryWrapper){
		return getDao().selectCount(queryWrapper);
	}
	
	protected Integer selectCountByEntity(E entity){
		return getDao().selectCount(new QueryWrapper<E>(entity));
	}
	
	protected void delete(Wrapper<E> queryWrapper){
		getDao().delete(queryWrapper);
	}
	
	protected int delByEntity(E entity){
		return this.getDao().delete(new QueryWrapper<E>(entity));
	}
	
	protected IWebPage<E> selectByPageNum(int pageNum, int pageDataNum, E e) {
		IPage<E> result = this.getDao().selectPage(new Page<E>(pageNum, pageDataNum),new QueryWrapper<E>(e));
		Long totalDataNum = new Long(this.selectCountByEntity(e));
		IWebPage<E> page = new IWebPage<E>(pageNum,totalDataNum.intValue(),pageDataNum,result.getRecords());
		return page;
	}
	
	protected IWebPage<E> selectByPageNumByQW(int pageNum, int pageDataNum, Wrapper<E> queryWrapper) {
		IPage<E> result = this.getDao().selectPage(new Page<E>(pageNum, pageDataNum),queryWrapper);
		Long totalDataNum = new Long(this.selectCount(queryWrapper));
		IWebPage<E> page = new IWebPage<E>(pageNum,totalDataNum.intValue(),pageDataNum,result.getRecords());
		return page;
	}
	
	protected List<E> selectByMaxIdDesc(Long maxId, int pageDataNum) {
		QueryWrapper<E> queryWrapper = new QueryWrapper<E>();
		queryWrapper.lt("id", maxId);
		queryWrapper.eq("status", 1L);
		queryWrapper.orderByDesc("id");
		IPage<E> result = this.getDao().selectPage(new Page<E>(0, pageDataNum),queryWrapper);
		return result.getRecords();
	}

	protected E selectMaxId() {
		QueryWrapper<E> queryWrapper = new QueryWrapper<E>();
		queryWrapper.orderByDesc("id");
		IPage<E> result = this.getDao().selectPage(new Page<E>(0, 1),queryWrapper);
		if (CollectionUtils.isEmpty(result.getRecords())) {
			return null;
		}
		return result.getRecords().get(0);
	}
	

	
	
}
