package com.codecate.wiki.common.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.springframework.web.multipart.MultipartFile;

public class IFileUtils {

	public static String getMd5ByFile(File file) {
		String value = null;
		FileInputStream in = null;
		try {
			in = new FileInputStream(file);
			MappedByteBuffer byteBuffer = in.getChannel().map(FileChannel.MapMode.READ_ONLY, 0, file.length());
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			md5.update(byteBuffer);
			BigInteger bi = new BigInteger(1, md5.digest());
			value = bi.toString(16);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != in) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return value;
	}
	
	/** 
     * 获取上传文件的md5 
     *  
     * @param file 
     * @return 
     * @throws NoSuchAlgorithmException 
     * @throws IOException 
     */  
    public static String getMd5ByMultipartFile(MultipartFile file) {  
  
        try {  
            byte[] uploadBytes = file.getBytes();  
            MessageDigest md5 = MessageDigest.getInstance("MD5");  
            byte[] digest = md5.digest(uploadBytes);  
            String hashString = new BigInteger(1, digest).toString(16);  
            return hashString;  
        } catch (Exception e) {  
           e.printStackTrace();
        }  
        return null;  
  
    }

	public static String getMd5ByInputStream(InputStream in) {
		String value = null;
		try {
			byte[] b = IOUtils.toByteArray(in);
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			md5.update(b);
			BigInteger bi = new BigInteger(1, md5.digest());
			value = bi.toString(16);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != in) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return value;
	}

	/**
	 * 获得文件类型
	 */
	public final static Map<String, String> FILE_TYPE_MAP = new HashMap<String, String>();

	static {
		getAllFileType(); // 初始化文件类型信息
	}

	/**
	 * Discription:[getAllFileType,常见文件头信息]
	 */
	private static void getAllFileType() {
		FILE_TYPE_MAP.put("ffd8ffe000104a464946", "jpg"); // JPEG (jpg)
		FILE_TYPE_MAP.put("89504e470d0a1a0a0000", "png"); // PNG (png)
		FILE_TYPE_MAP.put("47494638396126026f01", "gif"); // GIF (gif)
		FILE_TYPE_MAP.put("49492a00227105008037", "tif"); // TIFF (tif)
		FILE_TYPE_MAP.put("424d228c010000000000", "bmp"); // 16色位图(bmp)
		FILE_TYPE_MAP.put("424d8240090000000000", "bmp"); // 24位位图(bmp)
		FILE_TYPE_MAP.put("424d8e1b030000000000", "bmp"); // 256色位图(bmp)
		FILE_TYPE_MAP.put("41433130313500000000", "dwg"); // CAD (dwg)
		FILE_TYPE_MAP.put("3c21444f435459504520", "html"); // HTML (html)
		FILE_TYPE_MAP.put("3c21646f637479706520", "htm"); // HTM (htm)
		FILE_TYPE_MAP.put("48544d4c207b0d0a0942", "css"); // css
		FILE_TYPE_MAP.put("696b2e71623d696b2e71", "js"); // js
		FILE_TYPE_MAP.put("7b5c727466315c616e73", "rtf"); // Rich Text Format
															// (rtf)
		FILE_TYPE_MAP.put("38425053000100000000", "psd"); // Photoshop (psd)
		FILE_TYPE_MAP.put("46726f6d3a203d3f6762", "eml"); // Email [Outlook
															// Express 6] (eml)
		FILE_TYPE_MAP.put("d0cf11e0a1b11ae10000", "doc"); // MS Excel
															// 注意：word、msi 和
															// excel的文件头一样
		FILE_TYPE_MAP.put("d0cf11e0a1b11ae10000", "vsd"); // Visio 绘图
		FILE_TYPE_MAP.put("5374616E64617264204A", "mdb"); // MS Access (mdb)
		FILE_TYPE_MAP.put("252150532D41646F6265", "ps");
		FILE_TYPE_MAP.put("255044462d312e350d0a", "pdf"); // Adobe Acrobat (pdf)
		FILE_TYPE_MAP.put("2e524d46000000120001", "rmvb"); // rmvb/rm相同
		FILE_TYPE_MAP.put("464c5601050000000900", "flv"); // flv与f4v相同
		FILE_TYPE_MAP.put("00000020667479706d70", "mp4");
		FILE_TYPE_MAP.put("49443303000000002176", "mp3");// 49443303000000000776
		FILE_TYPE_MAP.put("000001ba210001000180", "mpg"); //
		FILE_TYPE_MAP.put("3026b2758e66cf11a6d9", "wmv"); // wmv与asf相同
		FILE_TYPE_MAP.put("52494646e27807005741", "wav"); // Wave (wav)
		FILE_TYPE_MAP.put("52494646d07d60074156", "avi");
		FILE_TYPE_MAP.put("4d546864000000060001", "mid"); // MIDI (mid)
		FILE_TYPE_MAP.put("504b0304140000000800", "zip");
		FILE_TYPE_MAP.put("526172211a0700cf9073", "rar");
		FILE_TYPE_MAP.put("235468697320636f6e66", "ini");
		FILE_TYPE_MAP.put("504b03040a0000000000", "jar");
		FILE_TYPE_MAP.put("4d5a9000030000000400", "exe");// 可执行文件
		FILE_TYPE_MAP.put("3c25402070616765206c", "jsp");// jsp文件
		FILE_TYPE_MAP.put("4d616e69666573742d56", "mf");// MF文件
		FILE_TYPE_MAP.put("3c3f786d6c2076657273", "xml");// xml文件
		FILE_TYPE_MAP.put("494e5345525420494e54", "sql");// xml文件
		FILE_TYPE_MAP.put("7061636b616765207765", "java");// java文件
		FILE_TYPE_MAP.put("406563686f206f66660d", "bat");// bat文件
		FILE_TYPE_MAP.put("1f8b0800000000000000", "gz");// gz文件
		FILE_TYPE_MAP.put("6c6f67346a2e726f6f74", "properties");// bat文件
		FILE_TYPE_MAP.put("cafebabe0000002e0041", "class");// bat文件
		FILE_TYPE_MAP.put("49545346030000006000", "chm");// bat文件
		FILE_TYPE_MAP.put("04000000010000001300", "mxp");// bat文件
		FILE_TYPE_MAP.put("504b0304140006000800", "docx");// docx文件
		FILE_TYPE_MAP.put("d0cf11e0a1b11ae10000", "wps");// WPS文字wps、表格et、演示dps都是一样的
		FILE_TYPE_MAP.put("6431303a637265617465", "torrent");
		FILE_TYPE_MAP.put("6D6F6F76", "mov"); // Quicktime (mov)
		FILE_TYPE_MAP.put("FF575043", "wpd"); // WordPerfect (wpd)
		FILE_TYPE_MAP.put("CFAD12FEC5FD746F", "dbx"); // Outlook Express (dbx)
		FILE_TYPE_MAP.put("2142444E", "pst"); // Outlook (pst)
		FILE_TYPE_MAP.put("AC9EBD8F", "qdf"); // Quicken (qdf)
		FILE_TYPE_MAP.put("E3828596", "pwl"); // Windows Password (pwl)
		FILE_TYPE_MAP.put("2E7261FD", "ram"); // Real Audio (ram)
	}

	/**
	 * 得到上传文件的文件头
	 * 
	 * @param src
	 * @return
	 */
	private static String bytesToHexString(byte[] src) {
		StringBuilder stringBuilder = new StringBuilder();
		if (src == null || src.length <= 0) {
			return null;
		}
		for (int i = 0; i < src.length; i++) {
			int v = src[i] & 0xFF;
			String hv = Integer.toHexString(v);
			if (hv.length() < 2) {
				stringBuilder.append(0);
			}
			stringBuilder.append(hv);
		}
		return stringBuilder.toString();
	}

	/**
	 * 根据指定文件的文件头判断其文件类型
	 * 
	 * @param filePaht
	 * @return
	 */
	public static String getFileType(File file) {
		String res = null;
		try {
			@SuppressWarnings("resource")
			FileInputStream is = new FileInputStream(file);
			byte[] b = new byte[10];
			is.read(b, 0, b.length);
			String fileCode = bytesToHexString(b);
			for (String key : FILE_TYPE_MAP.keySet()) {
				if (key.toLowerCase().startsWith(fileCode.toLowerCase())
						|| fileCode.toLowerCase().startsWith(key.toLowerCase())) {
					res = FILE_TYPE_MAP.get(key);
					break;
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return res;
	}

	/**
	 * 通过文件扩展名判断文件类型
	 * 如果fileName为null, 根据file参数判断， 否则根据fileName判断
	 * 
	 * @param file
	 * @param fileName
	 * @return
	 */
	public static String getFileTypeSuffix(String fileName, File file) {
		
		if (fileName == null && file != null) {
			fileName = file.getName();
		} else if ((fileName == null && file == null) || ("".equals(fileName.trim()) && file == null)) {
			return "参数错误";
		}
		String fileType = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length()).toLowerCase();
		// 图片类型数组
		String img[] = { "bmp", "jpg", "jpeg", "png", "tiff", "gif", "pcx", "tga", "exif", "fpx", "svg", "psd", "cdr",
				"pcd", "dxf", "ufo", "eps", "ai", "raw", "wmf" };
		for (int i = 0; i < img.length; i++) {
			if (img[i].equals(fileType)) {
				return "图片";
			}
		}
		// 视频类型数组
		String video[] = { "mp4", "avi", "mov", "wmv", "asf", "navi", "3gp", "mkv", "f4v", "rmvb", "webm" };
		for (int i = 0; i < video.length; i++) {
			if (video[i].equals(fileType)) {
				return "视频";
			}
		}
		// 音频类型数组
		String music[] = { "mp3", "wma", "wav", "mod", "ra", "cd", "md", "asf", "aac", "vqf", "ape", "mid", "ogg",
				"m4a", "vqf" };
		for (int i = 0; i < music.length; i++) {
			if (music[i].equals(fileType)) {
				return "音频";
			}
		}
		// 文档类型数组
		String document[] = { "txt", "doc", "docx", "xls", "xlsx", "htm", "html", "jsp", "rtf", "wpd", "pdf", "ppt" };
		for (int i = 0; i < document.length; i++) {
			if (document[i].equals(fileType)) {
				return "文档";
			}
		}
		return "其他";
	}
	
	public static boolean isSupportVideo(String fileType) {
		// 视频类型数组
		String video[] = { "mp4", "avi", "mov", "wmv", "asf", "navi", "3gp", "mkv", "f4v", "rmvb", "webm" };
		for (int i = 0; i < video.length; i++) {
			if (video[i].equalsIgnoreCase(fileType)) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean isSupportAudio(String fileType) {
		// 音频类型数组
		String music[] = { "mp3", "wma", "wav", "mod", "ra", "cd", "md", "asf", "aac", "vqf", "ape", "mid", "ogg",
				"m4a", "vqf" };
		for (int i = 0; i < music.length; i++) {
			if (music[i].equalsIgnoreCase(fileType)) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean isSupportImg(String fileType) {
		// 图片类型数组
		String img[] = { "bmp", "jpg", "jpeg", "png", "tiff", "gif", "pcx", "tga", "exif", "fpx", "svg", "psd", "cdr",
				"pcd", "dxf", "ufo", "eps", "ai", "raw", "wmf" };
		for (int i = 0; i < img.length; i++) {
			if (img[i].equalsIgnoreCase(fileType)) {
				return true;
			}
		}
		return false;
	}
	
	public static void delDir (File file){
		for (File sub : file.listFiles()) {
			if(sub.isDirectory()){
				IFileUtils.delDir(sub);
				sub.delete();
			}else{
				sub.delete();
			}
		}
		file.delete();
	}
	
	
	public static String readTextByFile (File file) throws Exception {
        String result = "";
        if(null!=file){
            List<String> textList = Files.readAllLines(Paths.get(file.getAbsolutePath()));
            for (String string : textList) {
                result += string;
            }
        }else {
            throw new Exception();
        }
        return result;
    }
	
	public static String readTextToHtmlByFile (File file) throws Exception {
        String result = "";
        if(null!=file){
            List<String> textList = Files.readAllLines(Paths.get(file.getAbsolutePath()));
            result += "<p>";
            
            for (String string : textList) {
                result += "```";
                result += string ;
                result += "``` ";
                result += "\r\n";
                
//                result += "</br>";
            }
            result += "</p>";
        }else {
            throw new Exception();
        }
        return result;
    }
	
	public static String readFileContent(File file) {
	    BufferedReader reader = null;
	    StringBuffer sbf = new StringBuffer();
	    try {
	        reader = new BufferedReader(new FileReader(file));
	        String tempStr;
	        while ((tempStr = reader.readLine()) != null) {
	            sbf.append(tempStr);
	            sbf.append("\r\n");
	        }
	        reader.close();
	        return sbf.toString();
	    } catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	        if (reader != null) {
	            try {
	                reader.close();
	            } catch (IOException e1) {
	                e1.printStackTrace();
	            }
	        }
	    }
	    return sbf.toString();
	}
	
}
