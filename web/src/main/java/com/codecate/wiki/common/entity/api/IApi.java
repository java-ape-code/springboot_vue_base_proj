package com.codecate.wiki.common.entity.api;

import java.util.List;

import lombok.Data;

@Data
public class IApi {

	private Long id;
	private String url;
	private List<IApiEnv> envs;
	private String method;
	private List<IApiHeader> headers;
	private List<IApiParam> params;
	
	
}
