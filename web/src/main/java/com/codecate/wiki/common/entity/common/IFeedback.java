package com.codecate.wiki.common.entity.common;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.codecate.ccfun.mb2ddl.annotation.Column;
import com.codecate.ccfun.mb2ddl.constants.Constants.MySqlType;

import lombok.Data;

@Data
@TableName("t_feedback")
public class IFeedback {

    @TableId(type = IdType.AUTO)
    @Column(type = MySqlType.INT, length = 11, isAutoIncrement = true)
    private Long id;
    @Column(type = MySqlType.VARCHAR, length = 30, isNotNull = true)
    private String appKey;
    @Column(type = MySqlType.VARCHAR, length = 2000, isNotNull = true)
    private String content;

}
