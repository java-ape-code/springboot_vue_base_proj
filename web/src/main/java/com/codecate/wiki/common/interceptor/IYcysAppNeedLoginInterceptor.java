package com.codecate.wiki.common.interceptor;


import java.io.OutputStreamWriter;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.codecate.wiki.common.model.common.IRespStatus;

@Component
public class IYcysAppNeedLoginInterceptor implements HandlerInterceptor{
    

    
	@Override
	public void afterCompletion(HttpServletRequest req, HttpServletResponse resp, Object arg2, Exception arg3)
			throws Exception {
		
	}

	@Override
	public void postHandle(HttpServletRequest req, HttpServletResponse resp, Object arg2, ModelAndView arg3)
			throws Exception {
		
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object arg2) throws Exception {
        boolean checkLogin = false;
		HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse rep = (HttpServletResponse) response;
		String token = req.getHeader("token");		
        String userIdStr = req.getHeader("id");
        Long userId = 0L;
		if (!StringUtils.isBlank(userIdStr)) {
            try {
                userId = Long.parseLong(userIdStr);   
            } catch (Exception e) {
            }        
        }
		if(!StringUtils.isBlank(token) && null!=userId && 0!=userId.longValue()) {
			ServletContext context = request.getServletContext();
	        ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(context);
//	        YAppUserServiceImpl userService = ctx.getBean(YAppUserServiceImpl.class);		
//			YUser user = userService.checkUserIdAndToken(userId,token);
//			if(null!=user) {
//				checkLogin = true;
//				request.setAttribute(IConstants.YAppUser.LOGIN_YCYS_USER_SESSION_KEY, user);
//			}
		}
		if (!checkLogin) {
        	//设置允许跨域的配置
            // 这里填写你允许进行跨域的主机ip（正式上线时可以动态配置具体允许的域名和IP）
//            rep.setHeader("Access-Control-Allow-Origin", "*");
            // 允许的访问方法
//            rep.setHeader("Access-Control-Allow-Methods","POST, GET, PUT, OPTIONS, DELETE, PATCH");
            // Access-Control-Max-Age 用于 CORS 相关配置的缓存
//            rep.setHeader("Access-Control-Max-Age", "3600");
//            rep.setHeader("Access-Control-Allow-Headers","token,Origin, X-Requested-With, Content-Type, Accept");
            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/json; charset=utf-8");
        	PrintWriter writer = null;
            OutputStreamWriter osw = null;
            try {
                osw = new OutputStreamWriter(response.getOutputStream(),
                        "UTF-8");
                writer = new PrintWriter(osw, true);
                IRespStatus status = new IRespStatus();
                status.setFailStatus();
                status.setMsg("用户授权认证没有通过!");
                String jsonStr = status.toJsonString();
                writer.write(jsonStr);
                writer.flush();
                writer.close();
                osw.close();
            } catch (Throwable e) {
            	e.printStackTrace();
            } finally {
                if (null != writer) {
                    writer.close();
                }
                if (null != osw) {
                    osw.close();
                }
            }
        }
		return checkLogin;
	}
}