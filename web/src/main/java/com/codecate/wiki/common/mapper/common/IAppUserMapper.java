package com.codecate.wiki.common.mapper.common;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.codecate.wiki.common.entity.common.IAppUser;
import com.codecate.wiki.common.mapper.IBaseMapper;

public interface IAppUserMapper extends IBaseMapper<IAppUser> {

    @Select({"<script>select * from t_app_user " + 
			" where id in " + 
			"<foreach collection='userIdList' index='index' item='item' open='(' separator=',' close=')'>" + 
			"#{item}" + 
			"</foreach>"+
			"</script>"})
	public List<IAppUser> findUserByIds(@Param("userIdList")List<Long> userIdList);


}
