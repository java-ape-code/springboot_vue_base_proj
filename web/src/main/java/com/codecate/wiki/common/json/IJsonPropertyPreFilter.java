package com.codecate.wiki.common.json;

import com.alibaba.fastjson.serializer.JSONSerializer;
import com.alibaba.fastjson.serializer.PropertyPreFilter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import java.util.List;

public class IJsonPropertyPreFilter implements PropertyPreFilter {
    @Override
    public boolean apply(JSONSerializer serializer, Object object, String name) {
        if (null!=object && object instanceof IJsonCusReturnProperty){
            IJsonCusReturnProperty tmp = (IJsonCusReturnProperty) object;
            List<String> noReturnNameList = tmp.jsonCusNoReturnPropertyNameList();
            if (CollectionUtils.isEmpty(noReturnNameList)){
                return true;
            }
            for (String str:noReturnNameList) {
                if(StringUtils.equals(str,name)){
                    return false;
                }
            }
            return true;
        }
        return true;
    }
}
