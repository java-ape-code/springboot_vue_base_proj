package com.codecate.wiki.common.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;

import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipFile;
import org.apache.tools.zip.ZipOutputStream;

import com.github.junrar.Archive;
import com.github.junrar.rarfile.FileHeader;

public class ICompressUtils {

	private static final int BUFFEREDSIZE = 1024;

	private static boolean flag = false;

	/**
	 * 解压rar包的内容到指定的目录下，可以处理其文件夹下包含子文件夹的情况**
	 * 
	 * @param rarFilename 要解压的rar包文件
	 * @param outPath     解压后存放的根目录
	 * @throws Exception
	 */
	public static String unRar(String rarFileName, String outPath, boolean addTime) throws Exception {
		if (rarFileName == null || "".equals(rarFileName) || outPath == null || "".equals(outPath)) {
			return null;
		}
		String fNameT1 = rarFileName.trim();
		String fNameT2 = fNameT1.substring(fNameT1.lastIndexOf("/") + 1);
		String fileName = "";
		int dot = fNameT2.lastIndexOf('.');
		if ((dot > -1) && (dot < (fNameT2.length()))) {
			fileName = fNameT2.substring(0, dot);
		}
		if ("".equals(fileName)) {
			return null;
		}
		if (addTime) {
			String timeStamp = String.valueOf(System.currentTimeMillis());
			outPath = outPath + "/" + timeStamp;
		}
		outPath = outPath + "/" + fileName;
		File outFile = new File(outPath);
		if (!outFile.exists()) {
			outFile.mkdirs();
		}
		File rarFile = new File(rarFileName);
		Archive archive = null;
		FileOutputStream fos = null;
		try {
			archive = new Archive(rarFile);
			FileHeader fh = archive.nextFileHeader();
			File destFileName = null;
			if (null == fh) {
				outPath = "";
				throw new Exception("不能解析");
			}
			while (fh != null) {
				String compressFileName = "";
				if (fh.isUnicode()) {
					compressFileName = fh.getFileNameW().trim();
				} else {
					compressFileName = fh.getFileNameString().trim();
				}

				compressFileName = new String(compressFileName);

				compressFileName = compressFileName.replace("\\", "/");

				destFileName = new File(outPath + "/" + compressFileName);

				if (fh.isDirectory()) {
					if (!destFileName.exists()) {
						destFileName.mkdirs();
					}
					fh = archive.nextFileHeader();
					continue;
				}
				if (!destFileName.getParentFile().exists()) {
					destFileName.getParentFile().mkdirs();
				}
				fos = new FileOutputStream(destFileName);
				archive.extractFile(fh, fos);
				fos.close();
				fos = null;
				fh = archive.nextFileHeader();
			}
			archive.close();
			archive = null;
		} catch (Exception e) {
		    e.printStackTrace();
			outPath = "";
		} finally {
			if (fos != null) {
				try {
					fos.close();
					fos = null;
				} catch (Exception e) {
				}
			}
			if (archive != null) {
				try {
					archive.close();
					archive = null;
				} catch (Exception e) {
				}
			}
		}
		return outPath;
	}

	/**
	 * 解压zip包的内容到指定的目录下，可以处理其文件夹下包含子文件夹的情况**
	 * 
	 * @param zipFilename 要解压的zip包文件
	 * @param outPath     解压后存放的根目录
	 */
	public static String unZip(String zipFileName, String outPath, boolean addTime) {
		if (zipFileName == null || "".equals(zipFileName) || outPath == null || "".equals(outPath)) {
			return null;
		}
		String fNameT1 = zipFileName.trim();
		String fNameT2 = fNameT1.substring(fNameT1.lastIndexOf("/") + 1);
		String fileName = "";
		int dot = fNameT2.lastIndexOf('.');
		if ((dot > -1) && (dot < (fNameT2.length()))) {
			fileName = fNameT2.substring(0, dot);
		}
		if ("".equals(fileName)) {
			return null;
		}
		if (addTime) {
			String timeStamp = String.valueOf(System.currentTimeMillis());
			outPath = outPath + "/" + timeStamp;
		}
		outPath = outPath + "/" + fileName;
		File outFile = new File(outPath);
		if (!outFile.exists()) {
			outFile.mkdirs();
		}
		try {
			ZipFile zipFile = new ZipFile(zipFileName, "GBK");
			Enumeration<?> en = zipFile.getEntries();
			ZipEntry zipEntry = null;
			while (en.hasMoreElements()) {
				zipEntry = (ZipEntry) en.nextElement();
				if (zipEntry.isDirectory()) {
					String dirName = zipEntry.getName();
					dirName = dirName.substring(0, dirName.length() - 1);
					File f = new File(outFile.getPath() + File.separator + dirName);
					f.mkdirs();
				} else {
					String strFilePath = outFile.getPath() + File.separator + zipEntry.getName();
					File f = new File(strFilePath);
					if (!f.exists()) {
						String[] arrFolderName = zipEntry.getName().split("/");
						String strRealFolder = "";
						for (int i = 0; i < (arrFolderName.length - 1); i++) {
							strRealFolder += arrFolderName[i] + File.separator;
						}
						strRealFolder = outFile.getPath() + File.separator + strRealFolder;
						File tempDir = new File(strRealFolder);
						tempDir.mkdirs();
					}
					f.createNewFile();
					InputStream in = zipFile.getInputStream(zipEntry);
					FileOutputStream out = new FileOutputStream(f);
					try {
						int c;
						byte[] by = new byte[BUFFEREDSIZE];
						while ((c = in.read(by)) != -1) {
							out.write(by, 0, c);
						}
						out.flush();
					} catch (IOException e) {
						throw e;
					} finally {
						out.close();
						in.close();
					}
				}
			}
		} catch (Exception e) {
		    e.printStackTrace();
		}
		return outPath;
	}

	/**
	 * 解压zip包的内容到指定的目录下，可以处理其文件夹下包含子文件夹的情况**
	 * 
	 * @param zipFilename 要解压的zip包文件
	 * @param outPath     解压后存放的根目录
	 * @throws Exception
	 */
	public static void unZipContentToPath(String zipFileName, String outPath) throws Exception {
		if (zipFileName == null || "".equals(zipFileName) || outPath == null || "".equals(outPath)) {
			return;
		}
		String fNameT1 = zipFileName.trim();
		String fNameT2 = fNameT1.substring(fNameT1.lastIndexOf("/") + 1);
		String fileName = "";
		int dot = fNameT2.lastIndexOf('.');
		if ((dot > -1) && (dot < (fNameT2.length()))) {
			fileName = fNameT2.substring(0, dot);
		}
		if ("".equals(fileName)) {
			return;
		}

		File outFile = new File(outPath);
		if (outFile.exists()) {
			File[] fileList = outFile.listFiles();
			for (File tmp : fileList) {
				tmp.delete();
			}
			outFile.delete();
		}
		if (!outFile.exists()) {
			outFile.mkdirs();
		}
		ZipFile zipFile = new ZipFile(zipFileName, "GBK");
		Enumeration<?> en = zipFile.getEntries();
		ZipEntry zipEntry = null;
		while (en.hasMoreElements()) {
			zipEntry = (ZipEntry) en.nextElement();
			if (zipEntry.isDirectory()) {
				String dirName = zipEntry.getName();
				dirName = dirName.substring(0, dirName.length() - 1);
				File f = new File(outFile.getPath() + File.separator + dirName);
				f.mkdirs();
			} else {
				String strFilePath = outFile.getPath() + File.separator + zipEntry.getName();
				File f = new File(strFilePath);
				if (!f.exists()) {
					String[] arrFolderName = zipEntry.getName().split("/");
					String strRealFolder = "";
					for (int i = 0; i < (arrFolderName.length - 1); i++) {
						strRealFolder += arrFolderName[i] + File.separator;
					}
					strRealFolder = outFile.getPath() + File.separator + strRealFolder;
					File tempDir = new File(strRealFolder);
					tempDir.mkdirs();
				}
				f.createNewFile();
				InputStream in = zipFile.getInputStream(zipEntry);
				FileOutputStream out = new FileOutputStream(f);
				try {
					int c;
					byte[] by = new byte[BUFFEREDSIZE];
					while ((c = in.read(by)) != -1) {
						out.write(by, 0, c);
					}
					out.flush();
				} catch (IOException e) {
					throw e;
				} finally {
					out.close();
					in.close();
				}
			}
		}
	}

	/**
	 * 删除父文件夹
	 * 
	 * @param filePath 文件目录
	 * @return
	 */
	public static boolean deleteFolder(String filePath) {
		if (filePath == null || "".equals(filePath)) {
			return false;
		}
		String filePath1 = filePath.trim();
		String filePath2 = filePath1.substring(0, filePath1.lastIndexOf("/"));
		flag = false;
		File file = new File(filePath2);
		if (!file.exists()) {
			return flag;
		} else {
			if (file.isFile()) {
				return deleteFile(filePath2);
			} else {
				return deleteDirectory(filePath2);
			}
		}
	}

	/**
	 * 删除单个文件
	 * 
	 * @param filePath 被删除文件的文件名
	 * @return 单个文件删除成功返回true，否则返回false
	 */
	public static boolean deleteFile(String filePath) {
		flag = false;
		File file = new File(filePath);
		if (file.isFile() && file.exists()) {
			file.delete();
			flag = true;
		}
		return flag;
	}

	/**
	 * 删除目录（文件夹）以及目录下的文件
	 * 
	 * @param filePath 被删除目录的文件路径
	 * @return 目录删除成功返回true，否则返回false
	 */
	public static boolean deleteDirectory(String filePath) {
		if (!filePath.endsWith(File.separator)) {
			filePath = filePath + File.separator;
		}
		File dirFile = new File(filePath);
		if (!dirFile.exists() || !dirFile.isDirectory()) {
			return false;
		}
		flag = true;
		File[] files = dirFile.listFiles();
		for (int i = 0; i < files.length; i++) {
			if (files[i].isFile()) {
				flag = deleteFile(files[i].getAbsolutePath());
				if (!flag)
					break;
			} else {
				flag = deleteDirectory(files[i].getAbsolutePath());
				if (!flag) {
					break;
				}
			}
		}
		if (!flag) {
			return false;
		}
		if (dirFile.delete()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 解压缩
	 */
	public static String deCompress(String sourceFile, String destDir) throws Exception {
		String result = "";
		// 保证文件夹路径最后是"/"或者"\"
		char lastChar = destDir.charAt(destDir.length() - 1);
		if (lastChar != '/' && lastChar != '\\') {
			destDir += File.separator;
		}
		// 根据类型，进行相应的解压缩
		String type = sourceFile.substring(sourceFile.lastIndexOf(".") + 1);
		if (type.equals("zip")) {
			result = ICompressUtils.unZip(sourceFile, destDir, true);
		} else if (type.equals("rar")) {
			result = ICompressUtils.unRar(sourceFile, destDir, true);
		} else {
			throw new Exception("只支持zip和rar5版本以下格式的压缩包，请勿使用最新rar5进行压缩！");
		}
		return result;
	}

	/**
	 * 解压缩
	 */
	public static String deCompressNotTime(String sourceFile, String destDir) throws Exception {
		String result = "";
		// 保证文件夹路径最后是"/"或者"\"
		char lastChar = destDir.charAt(destDir.length() - 1);
		if (lastChar != '/' && lastChar != '\\') {
			destDir += File.separator;
		}
		// 根据类型，进行相应的解压缩
		String type = sourceFile.substring(sourceFile.lastIndexOf(".") + 1);
		if (type.equals("zip")) {
			result = ICompressUtils.unZip(sourceFile, destDir, false);
		} else if (type.equals("rar")) {
			result = ICompressUtils.unRar(sourceFile, destDir, false);
		} else {
			throw new Exception("只支持zip和rar5版本以下格式的压缩包，请勿使用最新rar5进行压缩！");
		}
		return result;
	}

	public static final String ENCODING_DEFAULT = "UTF-8";

	public static final int BUFFER_SIZE_DIFAULT = 128;

	public static void makeZip(String[] inFilePaths, String zipPath) throws Exception {
		makeZip(inFilePaths, zipPath, ENCODING_DEFAULT);
	}

	public static void makeZip(String[] inFilePaths, String zipPath, String encoding) throws Exception {
		File[] inFiles = new File[inFilePaths.length];
		for (int i = 0; i < inFilePaths.length; i++) {
			inFiles[i] = new File(inFilePaths[i]);
		}
		makeZip(inFiles, zipPath, encoding);
	}

	public static void makeZip(File[] inFiles, String zipPath) throws Exception {
		makeZip(inFiles, zipPath, ENCODING_DEFAULT);
	}

	public static void makeZip(File[] inFiles, String zipPath, String encoding) throws Exception {
		ZipOutputStream zipOut = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(zipPath)));
		zipOut.setEncoding(encoding);
		for (int i = 0; i < inFiles.length; i++) {
			File file = inFiles[i];
			if (null != file) {
				doZipFile(zipOut, file, file.getParent());
			}
		}
		zipOut.flush();
		zipOut.close();
	}

	private static void doZipFile(ZipOutputStream zipOut, File file, String dirPath) throws Exception {
		if (file.isFile()) {
			BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
			String zipName = file.getPath().substring(dirPath.length());
			while (zipName.charAt(0) == '\\' || zipName.charAt(0) == '/') {
				zipName = zipName.substring(1);
			}
			ZipEntry entry = new ZipEntry(zipName);
			zipOut.putNextEntry(entry);
			byte[] buff = new byte[BUFFER_SIZE_DIFAULT];
			int size;
			while ((size = bis.read(buff, 0, buff.length)) != -1) {
				zipOut.write(buff, 0, size);
			}
			zipOut.closeEntry();
			bis.close();
		} else {
			File[] subFiles = file.listFiles();
			for (File subFile : subFiles) {
				doZipFile(zipOut, subFile, dirPath);
			}
		}
	}

}
