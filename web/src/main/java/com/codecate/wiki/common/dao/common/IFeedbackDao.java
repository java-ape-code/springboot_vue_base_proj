package com.codecate.wiki.common.dao.common;

import org.springframework.stereotype.Component;

import com.codecate.wiki.common.dao.IBaseDao;
import com.codecate.wiki.common.entity.common.IFeedback;
import com.codecate.wiki.common.mapper.common.IFeedbackMapper;

/**
 * IFeedbackDao
 */
@Component
public class IFeedbackDao extends IBaseDao<IFeedbackMapper,IFeedback>{

    public void saveFeedback(IFeedback entity){
        this.insert(entity);
    }
    
}