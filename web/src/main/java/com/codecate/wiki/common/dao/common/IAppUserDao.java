package com.codecate.wiki.common.dao.common;

import java.util.List;

import org.springframework.stereotype.Component;

import com.codecate.wiki.common.dao.IBaseDao;
import com.codecate.wiki.common.entity.common.IAppUser;
import com.codecate.wiki.common.mapper.common.IAppUserMapper;
import com.codecate.wiki.common.model.common.IWebPage;
import com.codecate.wiki.common.utils.ITimeUtils;

@Component
public class IAppUserDao extends IBaseDao<IAppUserMapper,IAppUser> {

    public IAppUser findUserByMobileAndPwd (String mobile, String passwd){
        IAppUser entity = new IAppUser();
        entity.setMobile(mobile);
        entity.setPasswd(passwd);
        entity.setFlag(1L);
        return this.selectOneByEntity(entity);
    }

    public IAppUser findUserByIdAndToken(Long id,String token){
        IAppUser entity = new IAppUser();
        entity.setId(id);
        entity.setToken(token);
        entity.setFlag(1L);
        return this.selectOneByEntity(entity);

    }

    public IAppUser findUserByIdAndOldToken(Long id,String token){
        IAppUser entity = new IAppUser();
        entity.setId(id);
        entity.setOToken(token);
        entity.setFlag(1L);
        return this.selectOneByEntity(entity);

    }

    
    public IAppUser findUserByMobile(String mobile) {
        IAppUser select = new IAppUser();
        select.setMobile(mobile);
        return this.selectOneByEntity(select);
    }

    public IAppUser findUserById(Long userId) {
        return this.selectByPrimaryKey(userId);
    }

    public void updateUser(IAppUser user) {
        this.update(user);
    }

    public void saveUser(IAppUser user) {
        user.setCreateTime(ITimeUtils.currentTimeSecond());
        this.insert(user);
    }

	public void delUserById(Long id) {
        IAppUser entity = new IAppUser();
        entity.setId(id);
        entity.setFlag(0L);
        this.update(entity);

    }

	public IAppUser findUserByMobileAndPasswd(String mobile, String passwd) {
		IAppUser entity = new IAppUser();
        entity.setMobile(mobile);
        entity.setPasswd(passwd);
        return this.selectOneByEntity(entity);
    }
    
    public List<IAppUser> findUserByIds (List<Long> ids) {
        return this.baseMapper.findUserByIds(ids);
    }

	public IWebPage<IAppUser> robotAppUserList(int pageNum, int pageDataNum) {
        IAppUser user = new IAppUser();
        user.setType(2L);
        user.setFlag(1L);
		return this.selectByPageNum(pageNum, pageDataNum, user);
    }


    public List<IAppUser> robotAppUserList() {
        IAppUser user = new IAppUser();
        user.setType(2L);
        user.setFlag(1L);
		return this.selectListByEntity(user);
    }
    
    public IWebPage<IAppUser> realAppUserList(int pageNum, int pageDataNum) {
        IAppUser user = new IAppUser();
        user.setType(1L);
        user.setFlag(1L);
		return this.selectByPageNum(pageNum, pageDataNum, user);
	}

}