package com.codecate.wiki.common.utils;

import java.text.SimpleDateFormat;
import java.util.Date;


public class ITimeUtils {
	public static Long currentTimeSecond() {
		return System.currentTimeMillis()/1000;
	}
	
	public static Date dateFromTimeSecond(Long time) {
		return new Date(time.longValue()*1000);
	}
	
	public static String dateStrFromTimeSecond(Long time) {
		String format = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat sdf = new SimpleDateFormat(format );  
		return sdf.format(new Date(time.longValue()*1000));
	}
	
	public static String dateStrFromTimeSecond(String format, Long time) {
		SimpleDateFormat sdf = new SimpleDateFormat(format );  
		return sdf.format(new Date(time.longValue()*1000));
	}
	
	public static String dateStrFromCurrTime(String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format );  
		return sdf.format(new Date(System.currentTimeMillis()));
	}
	
	public static String dateStrFromLastDay(String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format );  
		Long currTimeSecond = ITimeUtils.currentTimeSecond();
        Long lastTime = currTimeSecond.longValue() - 60*60*24;
        return sdf.format(new Date(lastTime.longValue()*1000));
		
	}
	
	public static Long timeFromDateStr(String format, String dateStr) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		Date date = sdf.parse(dateStr);
		return date.getTime();
	}
	
	public static Long timeSecondFromDateStr(String format, String dateStr) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		Date date = sdf.parse(dateStr);
		return date.getTime()/1000;
	}
	
	public static Long timeSecondFromDate(Date date) {
		return date.getTime()/1000;
	}
	
	public static Date dateFromDateStr(String format, String dateStr) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		Date date = sdf.parse(dateStr);
		return date;
	}
	
	public static Long timeSecondTodayStartTime() throws Exception {
		Long currTimeSecond = ITimeUtils.currentTimeSecond();
		String todayTimeStr = ITimeUtils.dateStrFromTimeSecond("yyyy-MM-dd", currTimeSecond);
		Long todayTime = ITimeUtils.timeSecondFromDateStr("yyyy-MM-dd-HH-mm", todayTimeStr + "-00-00");
		return todayTime;
	}

    public static Long timeSecondLastDayStartTime() throws Exception {
        Long currTimeSecond = ITimeUtils.currentTimeSecond();
        String todayTimeStr = ITimeUtils.dateStrFromTimeSecond("yyyy-MM-dd", currTimeSecond);
        Long todayTime = ITimeUtils.timeSecondFromDateStr("yyyy-MM-dd-HH-mm", todayTimeStr + "-00-00");
        Long lastTime = todayTime.longValue() - 60*60*24;
        return lastTime;
    }
	
	public static Long timeSecondCurrMonthStartTime() throws Exception {
		Long currTimeSecond = ITimeUtils.currentTimeSecond();
		String todayTimeStr = ITimeUtils.dateStrFromTimeSecond("yyyy-MM", currTimeSecond);
		Long todayTime = ITimeUtils.timeSecondFromDateStr("yyyy-MM-dd-HH-mm", todayTimeStr + "-01-00-00");
		return todayTime;
	}
	
	public static Long timeSecondTomorrowStartTime() throws Exception {
		Long currTimeSecond = ITimeUtils.currentTimeSecond();
		String tomorrowTimeStr = ITimeUtils.dateStrFromTimeSecond("yyyy-MM-dd", currTimeSecond+60*60*24-1);
		Long tomorrowTime = ITimeUtils.timeSecondFromDateStr("yyyy-MM-dd-HH-mm", tomorrowTimeStr + "-00-00");
		return tomorrowTime;
	}
}
