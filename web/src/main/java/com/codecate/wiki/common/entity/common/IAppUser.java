package com.codecate.wiki.common.entity.common;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.codecate.ccfun.mb2ddl.annotation.Column;
import com.codecate.ccfun.mb2ddl.annotation.Unique;
import com.codecate.ccfun.mb2ddl.constants.Constants.MySqlType;

import lombok.Data;

@Data
@TableName("t_app_user")
public class IAppUser {
    
    @TableId(type=IdType.AUTO)
    @Column(type = MySqlType.INT, length = 11, isAutoIncrement = true)
    private Long id;
    @Unique
    @Column(type = MySqlType.VARCHAR, length = 45, isNotNull = true)
    private String mobile;
    @Column(type = MySqlType.VARCHAR, length = 45)
    private String token;
    @Column(type = MySqlType.VARCHAR, length = 45)
    private String oToken;
    @Column(type = MySqlType.VARCHAR, length = 45)
    private String name;
    @Column(type = MySqlType.VARCHAR, length = 45)
    private String passwd;
    @Column(type = MySqlType.VARCHAR, length = 10,isNotNull = true)
    private String source;
    @Column(type = MySqlType.INT, length = 1,isNotNull = true)
    private Long type;
    @Column(type = MySqlType.INT, length = 11,isNotNull = true)
    private Long createTime;
    @Column(type = MySqlType.INT, length = 11)
    private Long registerTime;
    @Column(type = MySqlType.INT, length = 11)
    private Long loginTime;
    @Column(type = MySqlType.INT, length = 1,isNotNull = true)
    private Long flag;
    @Column(type = MySqlType.VARCHAR, length = 200)
    private String headImgPath;
    @Column(type = MySqlType.VARCHAR, length = 200)
    private String descript;

    @TableField(exist = false)
    private String headImgUrl;

}
