package com.codecate.wiki.common.utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.RandomAccessFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;  
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;


public class IHttpFileUtils {
	 
    /** 
     * 断点续传支持 
     * @param file 
     * @param request 
     * @param response 
     * @return 跳过多少字节 
     */  
    public static long headerSetting(File file,String fileName,HttpServletRequest request, HttpServletResponse response) {  
        long len = file.length();//文件长度
        String tmpFileName = file.getName();
        if(StringUtils.isNotBlank(fileName)){
            tmpFileName = fileName;
        }
        if ( null == request.getHeader("Range") ){  
            setResponse(new IRangeSettings(len),tmpFileName,response);  
            return 0;  
        }  
        String range = request.getHeader("Range").replaceAll("bytes=", "");  
        IRangeSettings settings = getSettings(len,range);  
        setResponse(settings,tmpFileName,response);  
        return settings.getStart();  
    }  
  
    private static void setResponse(IRangeSettings settings,String fileName, HttpServletResponse response) {  
          
        response.addHeader("Content-Disposition", "attachment; filename=\"" + IoUtil.toUtf8String(fileName) + "\"");  
        response.setContentType( IoUtil.setContentType(fileName));// set the MIME type.  
          
        if (!settings.isRange())  
        {  
            response.addHeader("Content-Length", String.valueOf(settings.getTotalLength()));  
        }  
        else  
        {  
            long start = settings.getStart();  
            long end = settings.getEnd();  
            long contentLength = settings.getContentLength();  
              
            response.setStatus(javax.servlet.http.HttpServletResponse.SC_PARTIAL_CONTENT);  
      
            response.addHeader("Content-Length", String.valueOf(contentLength));  
              
            String contentRange = new StringBuffer("bytes ").append(start).append("-").append(end).append("/").append(settings.getTotalLength()).toString();  
            response.setHeader("Content-Range", contentRange);  
        }  
    }  
  
    private static IRangeSettings getSettings(long len, String range) {  
        long contentLength = 0;  
        long start = 0;  
        long end = 0;  
        if (range.startsWith("-"))// -500，最后500个  
        {  
             contentLength = Long.parseLong(range.substring(1));//要下载的量  
             end = len-1;  
             start = len - contentLength;  
        }  
        else if (range.endsWith("-"))//从哪个开始  
        {  
            start = Long.parseLong(range.replace("-", ""));  
            end = len -1;  
            contentLength = len - start;  
        }  
        else//从a到b  
        {  
            String[] se = range.split("-");  
            start = Long.parseLong(se[0]);  
            end = Long.parseLong(se[1]);  
            contentLength = end-start+1;  
        }  
        return new IRangeSettings(start,end,contentLength,len);  
    } 
    
    /** 
     * 文件下载 
     * @param request 
     * @param response 
     * @throws UnsupportedEncodingException  
     */  
    public static void download(File downloadFile,HttpServletRequest request,HttpServletResponse response) throws Exception {  
        IHttpFileUtils.downloadFileWithFileName(downloadFile,null,request,response);
    }  
      
        /** 
     * 文件下载 
     * @param request 
     * @param response 
     * @throws UnsupportedEncodingException  
     */  
    public static void downloadFileWithFileName(File downloadFile,String fileName,HttpServletRequest request,HttpServletResponse response) throws Exception {  
        long pos = IHttpFileUtils.headerSetting(downloadFile, fileName,request, response);  
//      log.info("跳过"+pos);  
        ServletOutputStream os = null;   
        BufferedOutputStream out = null;  
        RandomAccessFile raf = null;  
        byte b[] = new byte[1024];//暂存容器  
        try {  
            os = response.getOutputStream();  
            out = new BufferedOutputStream(os);  
            raf = new RandomAccessFile(downloadFile, "r");  
            raf.seek(pos);  
            try {  
                    int n = 0;  
                    while ((n = raf.read(b, 0, 1024)) != -1) {  
                        out.write(b, 0, n);  
                    }  
                    out.flush();  
            } catch(Exception ie) {  
            	
            }  
        } catch (Exception e) {  
        	
        } finally {  
            if (out != null) {  
                try {  
                    out.close();  
                } catch (Exception e) {  
                	
                }  
            }  
            if (raf != null) {  
                try {  
                    raf.close();  
                } catch (Exception e) {  

                }  
            }  
        }  
    }    

}
