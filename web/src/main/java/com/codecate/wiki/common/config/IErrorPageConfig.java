package com.codecate.wiki.common.config;

import org.springframework.boot.web.server.ErrorPage;
import org.springframework.boot.web.server.ErrorPageRegistrar;
import org.springframework.boot.web.server.ErrorPageRegistry;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;

/**
 * IErrorPageConfig
 */
@Configuration
public class IErrorPageConfig implements ErrorPageRegistrar {

    @Override
    public void registerErrorPages(ErrorPageRegistry registry) {
        // ErrorPage error403Page = new ErrorPage(HttpStatus.FORBIDDEN, "403");
        // registry.addErrorPages(error403Page);
        // ErrorPage error404Page = new ErrorPage(HttpStatus.NOT_FOUND, "404");
        // registry.addErrorPages(error404Page);
        // ErrorPage error500Page = new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR, "500");
        // registry.addErrorPages(error500Page);


    }

    
}