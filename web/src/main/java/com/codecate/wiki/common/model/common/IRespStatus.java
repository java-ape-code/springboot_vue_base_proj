package com.codecate.wiki.common.model.common;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Data;

@Data
public class IRespStatus {

	public String status;
	public String msg;
	public Map<String, Object> data = new HashMap<>();
	
	public static IRespStatus succStatus() {
	    IRespStatus result = new IRespStatus();
	    return result;
	}
	
	public static IRespStatus failStatus() {
	    IRespStatus result = new IRespStatus();
	    result.setFailStatus();
        return result;
    }
	
	public static IRespStatus failLoginStatus() {
        IRespStatus result = new IRespStatus();
        result.setFailLoginStatus();
        return result;
    }

	public IRespStatus() {
		this.setSuccStatus();
	}

	public void setSuccStatus() {
		this.setStatus("1");
		this.setMsg("请求成功");
	}
	
	public void setFailStatus() {
		this.setStatus("0");
		this.setMsg("请求失败");
	}

	public void setFailLoginStatus() {
		this.setStatus("3");
		this.setMsg("登陆失败");
	}

	public String toJsonString() {
		String result = "";
		ObjectMapper om = new ObjectMapper();
		om.setSerializationInclusion(Include.NON_NULL);
		try {
			result = om.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return result;
	}

}
