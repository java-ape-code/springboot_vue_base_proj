package com.codecate.wiki.common.constant;

public class IConstants {

	public static class AdminUser{
		public static String LOGIN_ADMIN_USER_SESSION_KEY = "LOGIN_ADMIN_USER_SESSION_KEY";
	}
	
	public static class AppUser{
		public static String LOGIN_USER_SESSION_KEY = "LOGIN_USER_SESSION_KEY";
	}


	public static class YAppUser{
		public static String LOGIN_YCYS_USER_SESSION_KEY = "LOGIN_YCYS_USER_SESSION_KEY";
	}
	
	
	public static class YAppAPIHeader {
		public static String HEADEER_DEVICE_ID_KEY = "device_id";
	}



}
