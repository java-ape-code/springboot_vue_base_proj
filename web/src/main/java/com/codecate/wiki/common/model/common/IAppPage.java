package com.codecate.wiki.common.model.common;

import java.util.List;

import lombok.Data;

/**
 * IAppPage
 */
@Data
public class IAppPage<E> {

    private Long lastId;
	private List<E> dataList;

	public IAppPage(Long lastId, List<E> list) {
        this.lastId = lastId;
		this.dataList = list;

	}
    
}