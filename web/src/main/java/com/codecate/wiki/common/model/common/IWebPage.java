package com.codecate.wiki.common.model.common;

import java.util.List;

import lombok.Data;

/**
 * IWebPage
 */
@Data
public class IWebPage<E> {

	private int pageNum;
	private int totalDataNum;
	private int totalPageNum;
	private int pageDataNum;
	private List<E> dataList;

	public IWebPage(int pageNum, int totalDataNum, int pageDataNum, List<E> list) {

		this.pageNum = pageNum;
		this.totalDataNum = totalDataNum;
		this.pageDataNum = pageDataNum;
		this.totalPageNum = (int)((totalDataNum+pageDataNum-1)/pageDataNum);
		this.dataList = list;

	}
    
}