package com.codecate.wiki.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface IBaseMapper<T> extends BaseMapper<T> {

}