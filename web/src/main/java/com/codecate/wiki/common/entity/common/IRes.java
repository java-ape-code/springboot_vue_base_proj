package com.codecate.wiki.common.entity.common;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.codecate.ccfun.mb2ddl.annotation.Column;
import com.codecate.ccfun.mb2ddl.constants.Constants.MySqlType;

import lombok.Data;

@Data
@TableName("t_res")
public class IRes {
    
	@TableId(type=IdType.AUTO)
	@Column(type = MySqlType.INT, length = 11, isAutoIncrement = true)
    private Long id;
	@Column(type = MySqlType.VARCHAR, length = 100,isNotNull = true)
    private String name;
	@Column(type = MySqlType.VARCHAR, length = 200,isNotNull = true)
    private String path;
	@Column(type = MySqlType.INT, length = 1,isNotNull = true)
    private Long flag;
	@Column(type = MySqlType.INT, length = 11,isNotNull = true)
    private Long createTime;
	@Column(type = MySqlType.INT, length = 11,isNotNull = true)
    private Long userId;
	@Column(type = MySqlType.INT, length = 11,isNotNull = true)
    private Long pFolderId;
	@Column(type = MySqlType.VARCHAR, length = 32,isNotNull = true)
    private String md5;
	@Column(type = MySqlType.INT, length = 11,isNotNull = true)
    private Long size;
	@Column(type = MySqlType.INT, length = 1,isNotNull = true)
    private Long type;
	@Column(type = MySqlType.INT, length = 1,isNotNull = true,defaultValue = "0")
    private Long coverFlag;
    @TableField(exist = false)
    private String url;
    
}