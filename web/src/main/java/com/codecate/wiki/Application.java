package com.codecate.wiki;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

import com.codecate.wiki.common.config.IMixPropertySourceFactory;

@SpringBootApplication
@ComponentScan(basePackages = { "com.codecate" })
@PropertySources({
//        @PropertySource(value = "classpath:application-common.properties", factory = IMixPropertySourceFactory.class),
        @PropertySource(value = "classpath:application-common.yaml", factory = IMixPropertySourceFactory.class),
        @PropertySource(value = "classpath:application-${spring.profiles.active}.yaml", factory = IMixPropertySourceFactory.class) })
@MapperScan({ "com.codecate.wiki.common.mapper", "com.codecate.ccfun.mb2ddl.dao" })
public class Application extends SpringBootServletInitializer {
    

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(Application.class);
    }

    // @Bean
    // public Filter filter(){
    // ShallowEtagHeaderFilter filter=new ShallowEtagHeaderFilter();
    // return filter;
    // }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
