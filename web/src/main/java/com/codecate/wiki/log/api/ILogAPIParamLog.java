package com.codecate.wiki.log.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codecate.wiki.log.model.ILogEntity;

public class ILogAPIParamLog {

    private static Logger logger = LoggerFactory.getLogger(ILogAPIParamLog.class);

    public static void log(ILogEntity logEntity) {
        try {
            String logStr = logEntity.toLogStr();
            ILogAPIParamLog.logger.error(logStr);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
