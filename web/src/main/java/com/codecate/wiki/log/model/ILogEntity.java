package com.codecate.wiki.log.model;

import com.codecate.wiki.common.utils.ITimeUtils;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Data;

@Data
public class ILogEntity {

    private String type; // api
    private ILogEntityAPILog apiLog;
    private Long userId;
    private String deviceId;
    private String time;

    public static ILogEntity logAPIEntity(String apiUri, String apiName, Long userId, String deviceId) {
        ILogEntity result = new ILogEntity();
        result.type = "api";
        result.apiLog = new ILogEntityAPILog();
        result.apiLog.setUri(apiUri);
        result.apiLog.setName(apiName);
        result.setUserId(userId);
        result.setDeviceId(deviceId);
        result.setTime(ITimeUtils.dateStrFromCurrTime("yyyy-MM-dd HH:mm:ss"));
        return result;
    }

    public String toLogStr() {
        String result = "";
        ObjectMapper om = new ObjectMapper();
        om.setSerializationInclusion(Include.NON_NULL);
        try {
            result = om.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return result;
    }

}
