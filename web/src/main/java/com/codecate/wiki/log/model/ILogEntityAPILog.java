package com.codecate.wiki.log.model;

import lombok.Data;

@Data
public class ILogEntityAPILog {

    private String uri;
    private String name;
    private Long recipeId;
    private Long recipeCateId;
    private Long recipeTagId;
    private Long foodId;
    private Long foodTagId;
    private String key;
    private Long cardId;
    private Long cardSetsId;
    private Long lastId;

}
