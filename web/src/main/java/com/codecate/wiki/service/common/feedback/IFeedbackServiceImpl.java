package com.codecate.wiki.service.common.feedback;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.codecate.wiki.common.dao.common.IFeedbackDao;
import com.codecate.wiki.common.entity.common.IFeedback;

/**
 * IFeedbackImpl
 */
@Service
public class IFeedbackServiceImpl {

    @Autowired
    IFeedbackDao iFeedbackDao;

    public void save(String appKey, String content) {
        IFeedback entity = new IFeedback();
        entity.setAppKey(appKey);
        entity.setContent(content);
        this.iFeedbackDao.saveFeedback(entity);

    }

    
}