package com.codecate.wiki.service.common.res;

import java.io.File;
import java.math.BigInteger;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import com.codecate.wiki.common.dao.common.IResDao;
import com.codecate.wiki.common.entity.common.IRes;
import com.codecate.wiki.common.utils.IFileUtils;

/**
 * IResServiceImpl
 */
@Service
public class IResCreateServiceImpl {

    @Autowired
    IResDao iResDao;
    @Value("${cus.res.file.netbaseurl}")
    private String cusResFileNetBaseUrl;
	@Value("${cus.res.file.basepath}")
    private String cusResFileBasePath;

    public IRes saveFile(Long userId,File file,Long pId,String fileName) throws Exception {
        IRes res = new IRes();
        res.setUserId(userId);
        res.setPFolderId(pId);
        res.setFlag(1L);
        if(StringUtils.isBlank(fileName)){
            fileName = file.getName();
        }
        res.setName(fileName);
		String md5 = IFileUtils.getMd5ByFile(file);
		long fileL = file.length();
		IRes checkRes = checkResByMd5AndL(md5, fileL);
        if (null!=checkRes) {
			return checkRes;
		}
		res.setMd5(md5);
		BigInteger bi = new BigInteger(md5, 16);
		int fileValue = bi.mod(new BigInteger("100000000")).intValue();
		int aPath = fileValue / 10000 % 10000;
		int bPath = fileValue % 10000;
		String prefix = fileName.substring(fileName.lastIndexOf(".") + 1);
		String fileMd5Path = "" + aPath + "/" + bPath + "/" + md5 + "." + prefix;
        String fileSavePath = this.cusResFileBasePath + fileMd5Path;
		File saveFile = new File(fileSavePath);
        while (saveFile.exists()) {
            fileMd5Path = "" + aPath + "/" + bPath + "/" + md5 + "_" + System.currentTimeMillis() + "." + prefix;
            fileSavePath = this.cusResFileBasePath + fileMd5Path;
            saveFile = null;
            saveFile = new File(fileSavePath);
        }
		File parent = saveFile.getParentFile();
		if (parent != null && !parent.exists()) {
			parent.mkdirs();
		}
		if (!saveFile.exists()) {
			saveFile.createNewFile();
		}
		res.setPath(fileMd5Path);
		long type = 9; //未知
		if ("mp3".equalsIgnoreCase(prefix) || "wav".equalsIgnoreCase(prefix)) {
			type = 0;
		} else if ("mp4".equalsIgnoreCase(prefix) || "swf".equalsIgnoreCase(prefix)
				|| "flv".equalsIgnoreCase(prefix)) {
			type = 1;
		} else if ("png".equalsIgnoreCase(prefix) || "jpg".equalsIgnoreCase(prefix)
				|| "jpeg".equalsIgnoreCase(prefix)) {
			type = 2;
		} else if ("txt".equalsIgnoreCase(prefix) || "text".equalsIgnoreCase(prefix)) {
			type = 3;
		} else if ("json".equalsIgnoreCase(prefix)) {
			type = 6;
		} else if ("pdf".equalsIgnoreCase(prefix)) {
			type = 7;
		} else if ("xml".equalsIgnoreCase(prefix)) {
			type = 8;
		}
		if (9==type) {
		    throw new Exception();
        }
		res.setType(type);
        FileUtils.copyFile(file, saveFile);
        res.setSize(saveFile.length());
        this.iResDao.saveRes(res);
        res.setUrl(this.cusResFileNetBaseUrl+res.getPath());
		return res;
	}

    public IRes saveMultipartFile(Long userId,MultipartFile file,Long pId) throws Exception {
        IRes res = new IRes();
        res.setUserId(userId);
        res.setPFolderId(pId);
        res.setFlag(1L);
        String fileName = file.getOriginalFilename();
        res.setName(fileName);
        String md5 = IFileUtils.getMd5ByMultipartFile(file);
        long fileL = file.getSize();
        IRes checkRes = checkResByMd5AndL(md5, fileL);
        if (null!=checkRes) {
			return checkRes;
		}
        res.setMd5(md5);
        BigInteger bi = new BigInteger(md5, 16);
        int fileValue = bi.mod(new BigInteger("100000000")).intValue();
        int aPath = fileValue / 10000 % 10000;
        int bPath = fileValue % 10000;
        String prefix = fileName.substring(fileName.lastIndexOf(".") + 1);
        String fileMd5Path = "" + aPath + "/" + bPath + "/" + md5 + "." + prefix;
		
		String fileSavePath = this.cusResFileBasePath + fileMd5Path;
        File saveFile = new File(fileSavePath);
        while (saveFile.exists()) {
            fileMd5Path = ""+ aPath + "/" + bPath + "/" + md5 + "_" + System.currentTimeMillis() + "." + prefix;
            fileSavePath = this.cusResFileBasePath + fileMd5Path;
            saveFile = null;
            saveFile = new File(fileSavePath);
        }
        File parent = saveFile.getParentFile();
        if (parent != null && !parent.exists()) {
            parent.mkdirs();
        }
        if (!saveFile.exists()) {
            saveFile.createNewFile();
        }
        res.setPath(fileMd5Path);
        long type = 9; //未知
        if ("mp3".equalsIgnoreCase(prefix) || "wav".equalsIgnoreCase(prefix)) {
            type = 0;
        } else if ("mp4".equalsIgnoreCase(prefix) || "swf".equalsIgnoreCase(prefix)
                || "flv".equalsIgnoreCase(prefix)) {
            type = 1;
        } else if ("png".equalsIgnoreCase(prefix) || "jpg".equalsIgnoreCase(prefix)
                || "jpeg".equalsIgnoreCase(prefix)) {
            type = 2;
        } else if ("txt".equalsIgnoreCase(prefix) || "text".equalsIgnoreCase(prefix)) {
            type = 3;
        } else if ("json".equalsIgnoreCase(prefix)) {
            type = 6;
        } else if ("pdf".equalsIgnoreCase(prefix)) {
            type = 7;
        } else if ("xml".equalsIgnoreCase(prefix)) {
            type = 8;
        } else if ("gif".equalsIgnoreCase(prefix)) {
            type = 4;
        }
        if (9==type) {
            throw new Exception();
        }
        res.setType(type);
        FileUtils.copyInputStreamToFile(file.getInputStream(), saveFile);
        res.setSize(saveFile.length());
        this.iResDao.saveRes(res);
        return res;
    }
    
    public IRes checkResByMd5AndL(String md5,long length) {
    	List<IRes> resList = this.iResDao.findResByMd5(md5);
    	if (CollectionUtils.isEmpty(resList)) {
			return null;
		}
    	for (IRes iRes : resList) {
    		File f = new File(this.cusResFileBasePath + iRes.getPath());
    		if (null!=f && f.exists() && f.length()==length) {
				return iRes;
			}
    	}
    	return null;
    }
    
}
