package com.codecate.wiki.service.common.user;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.codecate.wiki.common.dao.common.IAppUserDao;
import com.codecate.wiki.common.entity.common.IAppUser;
import com.codecate.wiki.common.entity.common.IRes;
import com.codecate.wiki.common.utils.ITimeUtils;
import com.codecate.wiki.service.common.res.IResCreateServiceImpl;

import java.util.UUID;

@Component
public class IAppUserServiceImpl {

    @Autowired
    IAppUserDao iUserDao;
    @Autowired
    IAppUserServiceImpl iAppUserService;
    @Autowired
    IResCreateServiceImpl iResService;
    @Value("${cus.res.file.basepath}")
    private String cusResFileBasePath;
    @Value("${cus.res.file.netbaseurl}")
    private String resNetBaseUrl;

    public IAppUser userLogin(String mobile, String passwd) {
        IAppUser user = iUserDao.findUserByMobileAndPwd(mobile, passwd);
        if (null == user) {
            return user;
        }
        user.setLoginTime(ITimeUtils.currentTimeSecond());
        String token = UUID.randomUUID().toString().replaceAll("-", "");
        user.setToken(token);

        this.iUserDao.updateUser(user);
        if (StringUtils.isNotBlank(user.getHeadImgPath())) {
            user.setHeadImgUrl(this.resNetBaseUrl + user.getHeadImgPath());
        }
        return user;
    }

    public IAppUser tokenLogin(Long id, String token) {
        IAppUser user = iUserDao.findUserByIdAndToken(id, token);
        if (null == user) {
            return user;
        }
        user.setLoginTime(ITimeUtils.currentTimeSecond());
        user.setOToken(user.getToken());
        String ntoken = UUID.randomUUID().toString().replaceAll("-", "");
        user.setToken(ntoken);
        this.iUserDao.updateUser(user);
        if (StringUtils.isNotBlank(user.getHeadImgPath())) {
            user.setHeadImgUrl(this.resNetBaseUrl + user.getHeadImgPath());
        }
        return user;
    }

    public IAppUser register(String mobile, String passwd, String code) throws Exception {
        IAppUser user = iUserDao.findUserByMobile(mobile);
        
        if (StringUtils.equalsIgnoreCase(code, code)) {
            user.setFlag(1L);
            user.setPasswd(passwd);
            user.setRegisterTime(ITimeUtils.currentTimeSecond());
            user.setLoginTime(ITimeUtils.currentTimeSecond());
            String ntoken = UUID.randomUUID().toString().replaceAll("-", "");
            user.setOToken(ntoken);
            user.setToken(ntoken);
            this.iUserDao.updateUser(user);
        } else {
            throw new Exception();
        }
        return user;
    }

    public IAppUser info(IAppUser user) {
        if (StringUtils.isNotBlank(user.getHeadImgPath())) {
            user.setHeadImgUrl(this.resNetBaseUrl + user.getHeadImgPath());
        }
        return user;
    }

    public IAppUser checkUserIdAndToken(Long userId, String token) {
        IAppUser user =  this.iUserDao.findUserByIdAndToken(userId, token);
        if(null==user){
            user = this.iUserDao.findUserByIdAndOldToken(userId, token);
        }
        return user;
    }

    public void sendCode(String mobile, String deviceId) throws Exception {
        IAppUser user = this.iUserDao.findUserByMobile(mobile);
        if (null == user) {
            user = new IAppUser();
            user.setCreateTime(ITimeUtils.currentTimeSecond());
            user.setFlag(3L);
            user.setMobile(mobile);
            user.setSource("gif");
            user.setType(1L);
            this.iUserDao.saveUser(user);
        } else {
            if (3 != user.getFlag().longValue()) {
                throw new Exception();
            }

        }
    }

    public boolean changePwd(String mobile, String code, String pwd) throws Exception {
        IAppUser user = this.iUserDao.findUserByMobile(mobile);
        if (null == user) {
            return false;
        } else {
            if (1 != user.getFlag().longValue()) {
                return false;
            }
            
            if (StringUtils.equalsIgnoreCase(code, code)) {
                user.setPasswd(pwd);
                this.iUserDao.updateUser(user);
            } else {
                return false;
            }
        }
        return true;
    }

    public void updateUserInfo(IAppUser user, String desc, String name) {
        IAppUser update = new IAppUser();
        update.setId(user.getId());
        update.setDescript(desc);
        update.setName(name);
        this.iUserDao.updateUser(update);
    }

    public void updateUserHeadImg(IAppUser user, MultipartFile file) throws Exception {
        IRes res = this.iResService.saveMultipartFile(user.getId(), file, 0L);
        IAppUser update = new IAppUser();
        update.setId(user.getId());
        update.setHeadImgPath(res.getPath());
        this.iUserDao.updateUser(update);
    }



}
