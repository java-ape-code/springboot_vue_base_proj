package com.codecate.wiki.service.common.res;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.codecate.wiki.common.dao.common.IResDao;
import com.codecate.wiki.common.entity.common.IRes;
import com.codecate.wiki.common.model.common.IWebPage;

/**
 * IResListServiceImpl
 */
@Service
public class IResListServiceImpl {

    @Autowired
    IResDao iResDao;

    public IWebPage<IRes> resList(int pageNum, int pageDataNum){
        IWebPage<IRes> webPage = iResDao.findResList(pageNum, pageDataNum);
        if (!CollectionUtils.isEmpty(webPage.getDataList())) {
            for (IRes res : webPage.getDataList()) {
                res.setUrl("============");
            }
        }
        return webPage;
    }

    


    
    
}