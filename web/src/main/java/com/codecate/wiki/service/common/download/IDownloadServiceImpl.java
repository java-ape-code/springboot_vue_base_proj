package com.codecate.wiki.service.common.download;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import java.awt.*;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.Thumbnails.Builder;
import net.coobird.thumbnailator.geometry.Position;

/**
 * IDownloadServiceImpl
 */
@Service
public class IDownloadServiceImpl {

	@Value("${cus.res.file.basepath}")
	private String resourceBasePath;
	@Value("${cus.thumbnail.file.basepath}")
	private String thumbnailBasePath;

	@Value("${cover.img.path}")
	private String coverImgPath;

	public String changeImgSize(String path, Long width, Long height, String mode,String format) {
		String result = "";
		try {
			int scaleW = width.intValue();
			int scaleH = height.intValue();
			String addPath = path.toLowerCase();
			String[] pathArr = addPath.split("\\.");
			String[] floderArr = pathArr[0].split("/");
			int length = floderArr.length;
			String saveAddPathFolder = "";
			for (int i = 0; i < length; i++) {
				saveAddPathFolder = saveAddPathFolder + "/" + floderArr[i];
			}

			String fileName = "" + width + "_" + height + "_" + mode + ".webp";
			if (null!=format && "no".equalsIgnoreCase(format)) {
				fileName = "" + width + "_" + height + "_" + mode + ".jpg";
			}
			
			
			String savePath = this.thumbnailBasePath + saveAddPathFolder + "/" + fileName;

			if(addPath.endsWith(".gif_.webp")){
				savePath = this.thumbnailBasePath + addPath;
				if (null!=format && "no".equalsIgnoreCase(format)) {
					savePath = this.thumbnailBasePath + addPath;
					savePath = savePath.replace(".gif_.webp", ".gif");
				}
				
			}

			return savePath;

			// File saveFile = new File(savePath);
			// if (!saveFile.exists()) {

			// 	String filePath = this.resourceBasePath + path;
			// 	File f = new File(filePath);
			// 	BufferedImage bi;
			// 	bi = ImageIO.read(f);
			// 	int srcH = bi.getHeight();
			// 	int srcW = bi.getWidth();
			// 	int dstW = srcW;
			// 	int dstH = srcH;
			// 	if (mode.equalsIgnoreCase("width")) {
			// 		dstW = scaleW;
			// 		dstH = (int) ((float) width / (float) srcW * (float) srcH);
			// 	} else if (mode.equalsIgnoreCase("height")) {
			// 		dstW = (int) ((float) height / (float) srcH * (float) srcW);
			// 		dstH = scaleH;
			// 	} else if (mode.equalsIgnoreCase("all")) {
			// 		dstW = scaleW;
			// 		dstH = scaleH;
			// 	} else if (mode.equalsIgnoreCase("fit")) {
			// 		if (srcW > srcH) {
			// 			dstW = scaleW;
			// 			dstH = (int) ((float) width / (float) srcW * (float) srcH);
			// 		} else {
			// 			dstW = (int) ((float) height / (float) srcH * (float) srcW);
			// 			dstH = scaleH;
			// 		}
			// 	}

			// 	File parentFile = saveFile.getParentFile();
			// 	if (!parentFile.exists()) {
			// 		parentFile.mkdirs();
			// 	}
			// 	Builder<File> obj = Thumbnails.of(f).size(dstW, dstH);
			// 	String coverImgFilePath = "";
			// 	if (StringUtils.equalsIgnoreCase("debug", this.coverImgPath)) {
			// 		String staticPath = this.getClass().getClassLoader().getResource("static").getFile();
			// 		coverImgFilePath = staticPath + "/cover.png";
			// 	} else {
			// 		coverImgFilePath = this.coverImgPath;
			// 	}
			// 	BufferedImage coverImg = ImageIO.read(new File(coverImgFilePath));
			// 	obj.watermark(new Position() {
			// 		@Override
			// 		public Point calculate(int enclosingWidth, int enclosingHeight, int width, int height,
			// 				int insetLeft, int insetRight, int insetTop, int insetBottom) {
			// 			return new Point((enclosingWidth - 1000) / 2, (enclosingHeight - 1000) / 2);
			// 		}
			// 	}, coverImg, 1f);
			// 	obj.outputQuality(1).toFile(saveFile);
			// }
			// saveFile = null;
			// result = savePath;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

}