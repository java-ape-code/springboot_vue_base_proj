package com.codecate.wiki.quartz.job;

import org.quartz.CronScheduleBuilder;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;

@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public class IFixDataJob  implements Job{

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        try {

            
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public static JobDetail jobDetail() {
        return JobBuilder.newJob(IFixDataJob.class).withIdentity("IFixDataJobDetail").storeDurably().build();
    }

    public static Trigger jobTrigger() {
        return TriggerBuilder.newTrigger().forJob(IFixDataJob.jobDetail()).withIdentity("IFixDataJobTrigger")
                .withSchedule(CronScheduleBuilder.cronSchedule("*/5 * * * * ?")).build();
    }


}
