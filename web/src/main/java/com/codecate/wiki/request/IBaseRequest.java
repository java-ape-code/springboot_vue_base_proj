package com.codecate.wiki.request;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.socket.LayeredConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class IBaseRequest {

    public String dataCenterPostJson(String url, Map<String, Object> map) throws Exception {
        String result = "";
        if (!StringUtils.isBlank(url)) {
            if (url.startsWith("http://")) {
                result = this.postJson(url, map);
            } else if (url.startsWith("https://")) {
                result = this.postHttpsJson(url, map);
            }
        }
        return result;
    }

    public String dataCenterGet(String url, Map<String, Object> map) throws Exception {
        String result = "";
        if (!StringUtils.isBlank(url)) {
            if (url.startsWith("http://")) {
                result = this.get(url, map);
            } else if (url.startsWith("https://")) {
                result = this.getHttps(url, map);
            }
        }
        return result;
    }

    public String dataCenterPostFormData(String url, Map<String, Object> headerParams, Map<String, File> fileMap,
            Map<String, Object> params) {

        return "";
    }

    private String get(String url, Map<String, Object> map) throws Exception {
        String result = "";
        CloseableHttpClient httpclient = HttpClients.createDefault();
        String newUrl = url;
        if (null != map) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            for (String key : map.keySet()) {
                params.add(new BasicNameValuePair(key, (String) map.get(key)));
            }
            if (params.size() > 0) {
                newUrl = newUrl + "?" + EntityUtils.toString(new UrlEncodedFormEntity(params, Consts.UTF_8));
            }
        }
        HttpGet httpGet = new HttpGet(newUrl);
        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(50000).setConnectionRequestTimeout(10000)
                .setSocketTimeout(50000).build();
        httpGet.setConfig(requestConfig);
        CloseableHttpResponse response = httpclient.execute(httpGet);
        result = EntityUtils.toString(response.getEntity());
        response.close();
        return result;
    }

    public String getHttps(String url, Map<String, Object> map) throws Exception {
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            public void checkClientTrusted(X509Certificate[] certs, String authType) {
                // don't check
            }

            public void checkServerTrusted(X509Certificate[] certs, String authType) {
                // don't check
            }
        } };
        SSLContext ctx;
        try {
            ctx = SSLContext.getInstance("TLS");
            ctx.init(null, trustAllCerts, null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        LayeredConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(ctx);
        CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslSocketFactory).build();
        String newUrl = url;
        if (null != map) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            for (String key : map.keySet()) {
                params.add(new BasicNameValuePair(key, (String) map.get(key)));
            }
            if (params.size() > 0) {
                newUrl = newUrl + "?" + EntityUtils.toString(new UrlEncodedFormEntity(params, Consts.UTF_8));
            }
        }
        HttpGet httpGet = new HttpGet(newUrl);
        CloseableHttpResponse response = null;
        String httpStr = null;
        try {
            String requestJsonStr = this.map2Json(map);
            RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(50000)
                    .setConnectionRequestTimeout(10000).setSocketTimeout(50000).build();
            httpGet.setConfig(requestConfig);
            response = httpClient.execute(httpGet);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != HttpStatus.SC_OK) {
                HttpEntity entity = response.getEntity();
                return null;
            }
            HttpEntity entity = response.getEntity();
            if (entity == null) {
                return null;
            }
            httpStr = EntityUtils.toString(entity, "utf-8");
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            if (response != null) {
                try {
                    EntityUtils.consume(response.getEntity());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return httpStr;
    }

    public String postJson(String url, Map<String, Object> map) throws Exception {
        String result = "";
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);
        String requestJsonStr = this.map2Json(map);
        StringEntity entity = new StringEntity(requestJsonStr, "utf-8");
        entity.setContentEncoding("UTF-8");
        entity.setContentType("application/json");
        httpPost.setEntity(entity);
        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(50000).setConnectionRequestTimeout(10000)
                .setSocketTimeout(50000).build();
        httpPost.setConfig(requestConfig);
        CloseableHttpResponse response = httpclient.execute(httpPost);
        try {
            result = EntityUtils.toString(response.getEntity());
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            response.close();
        }
        return result;
    }

    private String postUrlEncode(String url, Long userId) throws Exception {
        String result = "";
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);
        StringEntity entity = new StringEntity("", "utf-8");
        entity.setContentEncoding("UTF-8");
        entity.setContentType("application/x-www-form-urlencoded");
        httpPost.setEntity(entity);
        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(50000).setConnectionRequestTimeout(10000)
                .setSocketTimeout(50000).build();
        httpPost.setConfig(requestConfig);
        CloseableHttpResponse response = httpclient.execute(httpPost);
        try {
            result = EntityUtils.toString(response.getEntity());
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            response.close();
        }
        return result;
    }

    private String getUrlEncode(String url, Long userId) throws Exception {
        String result = "";
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url);

        httpGet.setHeader("Content-Type", "application/x-www-form-urlencoded");
        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(50000).setConnectionRequestTimeout(10000)
                .setSocketTimeout(50000).build();
        httpGet.setConfig(requestConfig);
        CloseableHttpResponse response = httpclient.execute(httpGet);
        try {
            result = EntityUtils.toString(response.getEntity());
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            response.close();
        }
        return result;
    }

    public String postHttpsJson(String url, Map<String, Object> map) throws Exception {

        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            public void checkClientTrusted(X509Certificate[] certs, String authType) {
                // don't check
            }

            public void checkServerTrusted(X509Certificate[] certs, String authType) {
                // don't check
            }
        } };
        SSLContext ctx;
        try {
            ctx = SSLContext.getInstance("TLS");
            ctx.init(null, trustAllCerts, null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        LayeredConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(ctx);
        CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslSocketFactory).build();
        HttpPost httpPost = new HttpPost(url);
        CloseableHttpResponse response = null;
        String httpStr = null;
        try {
            String requestJsonStr = this.map2Json(map);
            StringEntity stringEntity = new StringEntity(requestJsonStr, "utf-8");// 解决中文乱码问题
            stringEntity.setContentEncoding("UTF-8");
            stringEntity.setContentType("application/json");
            httpPost.setEntity(stringEntity);
            RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(50000)
                    .setConnectionRequestTimeout(10000).setSocketTimeout(50000).build();
            httpPost.setConfig(requestConfig);
            response = httpClient.execute(httpPost);

            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != HttpStatus.SC_OK) {
                HttpEntity entity = response.getEntity();
                return null;
            }
            HttpEntity entity = response.getEntity();
            if (entity == null) {
                return null;
            }
            httpStr = EntityUtils.toString(entity, "utf-8");
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            if (response != null) {
                try {
                    EntityUtils.consume(response.getEntity());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return httpStr;
    }

    public String formData(String url, Map<String, Object> headerParams, Map<String, File> fileMap,
            Map<String, Object> params) throws Exception {
        String result = "";
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);
        if (null != headerParams && headerParams.values().size() > 0) {
            for (String key : headerParams.keySet()) {
                String str = (String) headerParams.get(key);
                httpPost.setHeader(key, str);
            }
        }
        MultipartEntityBuilder entity = MultipartEntityBuilder.create().setMode(HttpMultipartMode.RFC6532);
        entity.setCharset(Charset.forName(HTTP.UTF_8));
        if (null != fileMap && fileMap.values().size() > 0) {
            for (String key : fileMap.keySet()) {
                File file = fileMap.get(key);
                entity.addBinaryBody(key, file);
            }
        }
        if (null != params && params.values().size() > 0) {
            for (String key : params.keySet()) {
                Object str = (Object) params.get(key);
                entity.addTextBody(key, "" + str, ContentType.MULTIPART_FORM_DATA.withCharset(HTTP.UTF_8));
            }
        }
        httpPost.setEntity(entity.build());
        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(-1).setConnectionRequestTimeout(-1)
                .setSocketTimeout(-1).build();
        httpPost.setConfig(requestConfig);
        CloseableHttpResponse response = client.execute(httpPost);
        try {
            result = EntityUtils.toString(response.getEntity());
        } finally {
            response.close();
        }
        return result;
    }

    protected String map2Json(Map<String, Object> map) {
        String result = "";
        ObjectMapper om = new ObjectMapper();
        om.setSerializationInclusion(Include.NON_NULL);
        try {
            result = om.writeValueAsString(map);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return result;
    }

    protected String list2Json(List<Object> list) {
        String result = "";
        ObjectMapper om = new ObjectMapper();
        om.setSerializationInclusion(Include.NON_NULL);
        try {
            result = om.writeValueAsString(list);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return result;
    }

    protected Map<String, Object> json2Map(String jsonStr) throws Exception {
        Map<String, Object> result = new HashMap<String, Object>();
        ObjectMapper om = new ObjectMapper();
        result = om.readValue(jsonStr, new TypeReference<Map<String, Object>>() {
        });
        return result;
    }

}
