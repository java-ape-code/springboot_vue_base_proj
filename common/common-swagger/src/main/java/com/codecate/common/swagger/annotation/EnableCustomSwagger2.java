package com.codecate.common.swagger.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;

import com.codecate.common.swagger.config.SwaggerAutoConfiguration;


@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@AutoConfigureAfter(value = {SwaggerAutoConfiguration.class})
public @interface EnableCustomSwagger2
{

}
